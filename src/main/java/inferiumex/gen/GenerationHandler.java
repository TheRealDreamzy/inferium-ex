package inferiumex.gen;

import inferiumex.InferiumEx;
import inferiumex.blocks.BlockRegistry;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;
import org.apache.logging.log4j.Level;

import java.util.Random;

public class GenerationHandler implements IWorldGenerator
{
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider)
    {
        switch (world.provider.getDimension())
        {
            case -1:
                break;
            case 0:
                generateLaranium(world, random, chunkX * 16, chunkZ * 16);
                generateAmethyte(world, random, chunkX * 16, chunkZ * 16);
                generateToparite(world, random, chunkX * 16, chunkZ * 16);
                generateHearanium(world, random, chunkX * 16, chunkZ * 16);
                generateSapphirite(world, random, chunkX * 16, chunkZ * 16);
                generateCrystal(world, random, chunkX * 16, chunkZ * 16);
                break;
            case 1:
                break;
        }
    }

    private void generateLaranium(World world, Random rand, int chunkX, int chunkZ)
    {
        for (int k = 0; k < 4; k++)
        {
            int firstXCoord = chunkX + rand.nextInt(16);
            int firstZCoord = chunkZ + rand.nextInt(16);

            int getPosY = rand.nextInt(9);
            BlockPos orePos = new BlockPos(firstXCoord, getPosY, firstZCoord);
            (new WorldGenMinable(BlockRegistry.LARANIUM_ORE.getDefaultState(), 8)).generate(world, rand, orePos);
            InferiumEx.LOGGER.log(Level.DEBUG, "Generated laranium ore at chunks [" + firstXCoord + ", " + firstZCoord + "] at height Y: " + getPosY);
        }
    }

    private void generateAmethyte(World world, Random rand, int chunkX, int chunkZ)
    {
        for (int k = 0; k < 7; k++)
        {
            int xChunk = chunkX + rand.nextInt(16);
            int zChunk = chunkZ + rand.nextInt(16);

            int getPosY = rand.nextInt(26);
            BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk);
            (new WorldGenMinable(BlockRegistry.AMETHYTE_ORE.getDefaultState(), 8)).generate(world, rand, orePos);
            InferiumEx.LOGGER.log(Level.DEBUG, "Generated amethyte ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
        }
    }

    private void generateToparite(World world, Random rand, int chunkX, int chunkZ)
    {
        for (int k = 0; k < 9; k++)
        {
            int xChunk = chunkX + rand.nextInt(16);
            int zChunk = chunkZ + rand.nextInt(16);

            int getPosY = rand.nextInt(35);
            BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk);
            (new WorldGenMinable(BlockRegistry.TOPARITE_ORE.getDefaultState(), 10)).generate(world, rand, orePos);
            InferiumEx.LOGGER.log(Level.DEBUG, "Generated toparite ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
        }
    }

    private void generateHearanium(World world, Random rand, int chunkX, int chunkZ)
    {
        for (int k = 0; k < 8; k++)
        {
            int xChunk = chunkX + rand.nextInt(16);
            int zChunk = chunkZ + rand.nextInt(16);

            int getPosY = rand.nextInt(42);
            BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk);
            (new WorldGenMinable(BlockRegistry.HEARANIUM_ORE.getDefaultState(), 8)).generate(world, rand, orePos);
            InferiumEx.LOGGER.log(Level.DEBUG, "Generated hearanium ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
        }
    }

    private void generateSapphirite(World world, Random rand, int chunkX, int chunkZ)
    {
        for (int k = 0; k < 9; k++)
        {
            int xChunk = chunkX + rand.nextInt(16);
            int zChunk = chunkZ + rand.nextInt(16);

            int getPosY = rand.nextInt(53);
            BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk);
            (new WorldGenMinable(BlockRegistry.SAPPHIRITE_ORE.getDefaultState(), 10)).generate(world, rand, orePos);
            InferiumEx.LOGGER.log(Level.DEBUG, "Generated sapphirite ore at chunks [" + xChunk + ", " + zChunk + "] at height Y: " + getPosY);
        }
    }

    private void generateCrystal(World world, Random rand, int chunkX, int chunkZ)
    {
        for (int k = 0; k < 4; k++)
        {
            int xChunk = chunkX + rand.nextInt(16);
            int zChunk = chunkZ + rand.nextInt(16);

            int getPosY = rand.nextInt(12);
            BlockPos orePos = new BlockPos(xChunk, getPosY, zChunk);
            (new WorldGenMinable(BlockRegistry.CRYSTAL_ORE.getDefaultState(), 5)).generate(world, rand, orePos);
            InferiumEx.LOGGER.log(Level.DEBUG, "Generated crystal ore at chunks [" + xChunk + ", " + "] at height Y: " + getPosY);
        }
    }
}
