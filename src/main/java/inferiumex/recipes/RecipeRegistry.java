package inferiumex.recipes;

import inferiumex.blocks.BlockRegistry;
import inferiumex.items.ItemRegistry;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;

@Mod.EventBusSubscriber(modid = "inferiumex")
public class RecipeRegistry
{
    @SubscribeEvent
    public static void registerRecipes(RegistryEvent.Register<IRecipe> event)
    {
        GameRegistry.addSmelting(new ItemStack(BlockRegistry.LARANIUM_ORE, 1), new ItemStack(ItemRegistry.LARANIUM_INGOT, 1), 2.25F);
        GameRegistry.addSmelting(new ItemStack(BlockRegistry.AMETHYTE_ORE, 1), new ItemStack(ItemRegistry.AMETHYTE_INGOT, 1), 2.00F);
        GameRegistry.addSmelting(new ItemStack(BlockRegistry.TOPARITE_ORE, 1), new ItemStack(ItemRegistry.TOPARITE_INGOT, 1), 1.75F);
        GameRegistry.addSmelting(new ItemStack(BlockRegistry.HEARANIUM_ORE, 1), new ItemStack(ItemRegistry.HEARANIUM_INGOT,1), 1.25F);
        GameRegistry.addSmelting(new ItemStack(BlockRegistry.SAPPHIRITE_ORE, 1), new ItemStack(ItemRegistry.SAPPHIRITE_INGOT, 1), 1.00F);
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public static void registerOreDict(RegistryEvent.Register<IRecipe> event)
    {
        OreDictionary.registerOre("blockLaranium", new ItemStack(BlockRegistry.LARANIUM_BLOCK));
        OreDictionary.registerOre("blockAmethyte", new ItemStack(BlockRegistry.AMETHYTE_BLOCK));
        OreDictionary.registerOre("blockToparite", new ItemStack(BlockRegistry.TOPARITE_BLOCK));
        OreDictionary.registerOre("blockHearanium", new ItemStack(BlockRegistry.HEARANIUM_BLOCK));
        OreDictionary.registerOre("blockSapphirite", new ItemStack(BlockRegistry.SAPPHIRITE_BLOCK));
        OreDictionary.registerOre("blockCrystal", new ItemStack(BlockRegistry.CRYSTAL_BLOCK));
        OreDictionary.registerOre("oreLaranium", new ItemStack(BlockRegistry.LARANIUM_ORE));
        OreDictionary.registerOre("oreAmethyte", new ItemStack(BlockRegistry.AMETHYTE_ORE));
        OreDictionary.registerOre("oreToparite", new ItemStack(BlockRegistry.TOPARITE_ORE));
        OreDictionary.registerOre("oreHearanium", new ItemStack(BlockRegistry.HEARANIUM_ORE));
        OreDictionary.registerOre("oreSapphirite", new ItemStack(BlockRegistry.SAPPHIRITE_ORE));
        OreDictionary.registerOre("oreCrystal", new ItemStack(BlockRegistry.CRYSTAL_ORE));
        OreDictionary.registerOre("slabMetal", new ItemStack(BlockRegistry.METAL_SLAB));
        OreDictionary.registerOre("ingotLaranium", new ItemStack(ItemRegistry.LARANIUM_INGOT));
        OreDictionary.registerOre("ingotAmethyte", new ItemStack(ItemRegistry.AMETHYTE_INGOT));
        OreDictionary.registerOre("ingotToparite", new ItemStack(ItemRegistry.TOPARITE_INGOT));
        OreDictionary.registerOre("ingotHearanium", new ItemStack(ItemRegistry.HEARANIUM_INGOT));
        OreDictionary.registerOre("ingotSapphirite", new ItemStack(ItemRegistry.SAPPHIRITE_INGOT));
        OreDictionary.registerOre("nuggetLaranium", new ItemStack(ItemRegistry.LARANIUM_NUGGET));
        OreDictionary.registerOre("nuggetHearanium", new ItemStack(ItemRegistry.HEARANIUM_NUGGET));
        OreDictionary.registerOre("nuggetToparite", new ItemStack(ItemRegistry.TOPARITE_NUGGET));
        OreDictionary.registerOre("nuggetAmethyte", new ItemStack(ItemRegistry.AMETHYTE_NUGGET));
        OreDictionary.registerOre("nuggetSapphirite", new ItemStack(ItemRegistry.SAPPHIRITE_NUGGET));
        OreDictionary.registerOre("fragmentCrystal", new ItemStack(ItemRegistry.CRYSTAL_FRAGMENT));
        OreDictionary.registerOre("fragmentFrameCrystal", new ItemStack(ItemRegistry.CRYSTAL_FRAME_FRAGMENT));
        OreDictionary.registerOre("stickObsidian", new ItemStack(ItemRegistry.OBSIDIAN_STICK));
    }
}
