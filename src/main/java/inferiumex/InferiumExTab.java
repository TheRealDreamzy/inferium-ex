package inferiumex;

import inferiumex.blocks.BlockRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class InferiumExTab
{
    public static final CreativeTabs INFERIUM_EX_TAB = new CreativeTabs("inferiumex")
    {
        @Override
        @SideOnly(Side.CLIENT)
        public ItemStack getTabIconItem()
        {
           return new ItemStack(BlockRegistry.LARANIUM_ORE);
        }
    };
}
