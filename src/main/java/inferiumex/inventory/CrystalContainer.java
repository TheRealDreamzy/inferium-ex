package inferiumex.inventory;

import inferiumex.tileentity.CrystalBlockTileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class CrystalContainer extends Container
{
    public static final int xOffset = 143;
    public static final int tilexOffset = 8;
    public static final int yOffset = 194;
    public static final int tileyOffset = 18;
    public static final int slotBorder = 1;
    public static final int inventorySlotsVertical = 3;
    public static final int tileInventorySlotsVertical = 9;
    public static final int inventorySlotsHorizontal = 9;
    public static final int tileInventorySlotsHorizontal = 24;
    public static final int hotbarSlotsHorizontal = 9;
    public static final int hotbarSeparation = 4;

    public CrystalBlockTileEntity blockTileEntity;

    public CrystalContainer(EntityPlayer player, CrystalBlockTileEntity blockTileEntity)
    {
        this.blockTileEntity = blockTileEntity;
        addPlayerInventory(player);

        if (blockTileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null))
        {
            addTileEntityInventory();
        }
    }

     public void addPlayerInventory(EntityPlayer playerIn)
     {
         IInventory playerInventory = playerIn.inventory;
         for (int row = 0; row < inventorySlotsVertical; row++)
         {
             for (int col = 0; col < inventorySlotsHorizontal; col++)
             {
                 int x = xOffset + col * (16 + (slotBorder * 2));
                 int y = row * (16 + (slotBorder * 2)) + yOffset;

                 this.addSlotToContainer(new Slot(playerInventory, col + row * inventorySlotsHorizontal + hotbarSlotsHorizontal, x, y));
             }
         }

         int hotbarY = (inventorySlotsVertical * (16 + (slotBorder * 2))) + hotbarSeparation + yOffset;

         for (int col = 0; col < inventorySlotsHorizontal; col++)
         {
             int x = xOffset + col * (16 + (slotBorder * 2));
             this.addSlotToContainer(new Slot(playerInventory, col, x, hotbarY));
         }
     }

     public void addTileEntityInventory()
     {
         IItemHandler inventory = blockTileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);

         for (int row = 0; row < tileInventorySlotsVertical; row++)
         {
             for (int col = 0; col < tileInventorySlotsHorizontal; col++)
             {
                 int x = tilexOffset + col * (16 + (slotBorder * 2));
                 int y = row * (16 + (slotBorder * 2)) + tileyOffset;
                 addSlotToContainer(new SlotItemHandler(inventory, col + row * tileInventorySlotsHorizontal, x, y)
                 {
                   @Override
                   public void onSlotChanged()
                   {
                       blockTileEntity.markDirty();
                   }
                 });
             }
         }
     }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn)
    {
        return blockTileEntity.canInteractWith(playerIn);
    }
}
