package inferiumex.util;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;

public class MatRefs
{
    public static final Item.ToolMaterial LARANIUM = EnumHelper.addToolMaterial("inferiumex:laranium", 8, 2000, 11.0F, 8.0F, 23);
    public static final Item.ToolMaterial AMETHYTE = EnumHelper.addToolMaterial("inferiumex:amethyte", 7, 1900, 10.0F, 6.5F, 19);
    public static final Item.ToolMaterial TOPARITE = EnumHelper.addToolMaterial("inferiumex:toparite", 6, 1750, 9.0F, 6.0F, 16);
    public static final Item.ToolMaterial HEARANIUM = EnumHelper.addToolMaterial("inferiumex:hearanium", 5, 1500, 8.0F, 5.7F, 15);
    public static final Item.ToolMaterial SAPPHIRITE = EnumHelper.addToolMaterial("inferiumex:sapphirite", 4, 1350, 7.0F, 5.0F, 13);
}