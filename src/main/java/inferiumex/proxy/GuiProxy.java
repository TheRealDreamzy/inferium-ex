package inferiumex.proxy;

import inferiumex.blocks.CrystalBlock;
import inferiumex.client.gui.CBlockGuiContainer;
import inferiumex.inventory.CrystalContainer;
import inferiumex.tileentity.CrystalBlockTileEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

public class GuiProxy implements IGuiHandler
{
    public static final int CRYSTAL = 0;

    @Override
    @Nullable
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        BlockPos pos = new BlockPos(x, y, z);
        if (world.isBlockLoaded(pos))
        {
            TileEntity tileEntity = world.getTileEntity(pos);
            switch (ID)
            {
                case CRYSTAL:
                    if (tileEntity instanceof CrystalBlockTileEntity)
                    {
                        CrystalBlockTileEntity tileEntity1 = (CrystalBlockTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new CrystalContainer(player, tileEntity1);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return null;
    }

    @Override
    @Nullable
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        BlockPos pos = new BlockPos(x, y, z);
        if (world.isBlockLoaded(pos))
        {
            TileEntity tileEntity = world.getTileEntity(pos);
            switch (ID)
            {
                case CRYSTAL:
                    if (tileEntity instanceof CrystalBlockTileEntity)
                    {
                        CrystalBlockTileEntity tileEntity1 = (CrystalBlockTileEntity) tileEntity;
                        if (tileEntity1.canInteractWith(player))
                        {
                            return new CBlockGuiContainer(player, tileEntity1);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return null;
    }

}
