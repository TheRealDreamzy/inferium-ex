package inferiumex.client.gui;

import inferiumex.inventory.CrystalContainer;
import inferiumex.tileentity.CrystalBlockTileEntity;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class CBlockGuiContainer extends GuiContainer
{
    public IInventory playerIn;
    public static CrystalBlockTileEntity tileEntity;
    public static final ResourceLocation LEFT = new ResourceLocation("inferiumex:textures/gui/container/generic_216_left.png");
    public static final ResourceLocation RIGHT = new ResourceLocation("inferiumex:textures/gui/container/generic_216_right.png");
    public static final ResourceLocation PLAYER = new ResourceLocation("inferiumex:textures/gui/container/player_inventory.png");

    public CBlockGuiContainer(EntityPlayer player, CrystalBlockTileEntity blockTileEntity)
    {
        super(new CrystalContainer(player, blockTileEntity));
        tileEntity = blockTileEntity;
        playerIn = player.inventory;
        this.xSize = 446;
        this.ySize = 276;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
    {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;

        this.mc.getTextureManager().bindTexture(LEFT);
        this.drawTexturedModalRect(i, j, 0, 0, 223, 186);

        this.mc.getTextureManager().bindTexture(RIGHT);
        this.drawTexturedModalRect(i + 223, j, 0, 0, 223, 186);

        this.mc.getTextureManager().bindTexture(PLAYER);
        this.drawTexturedModalRect(i + 135, j + 186, 0, 0, 176, 90);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
    {
        String s = "Crystal Block";
        this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 6, 4210752);
        this.fontRenderer.drawString(this.playerIn.getDisplayName().getUnformattedText(), 143, this.ySize - 96 + 2, 4210752);
    }
}
