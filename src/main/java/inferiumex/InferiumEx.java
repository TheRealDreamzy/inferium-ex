package inferiumex;

import inferiumex.gen.GenerationHandler;
import inferiumex.proxy.GuiProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod(modid = "inferiumex", name = "Inferium Ex", version = "1.12.2-0.30.0_rc1", updateJSON = "https://gist.githubusercontent.com/zi8tx/b9f371fc75ee4b9de615f6f75f203c62/raw/update.json")
public class InferiumEx
{
    @Mod.Instance
    public static InferiumEx instance;

    public static final Logger LOGGER = LogManager.getLogger("inferiumex");

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        GameRegistry.registerWorldGenerator(new GenerationHandler(), 2);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiProxy());
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    }
}
