package inferiumex.blocks;

import inferiumex.InferiumExTab;
import inferiumex.items.ItemRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.IBlockAccess;

import java.util.Random;

public class CrystalOre extends Block
{
    public CrystalOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(3.0F);
        setResistance(15.0F);
        setHarvestLevel("pickaxe", 3);
        setRegistryName("crystal_ore");
        setUnlocalizedName("inferiumex.crystal_ore");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }

    @Override
    public int quantityDropped(IBlockState state, int fortune, Random random)
    {
        if (fortune > 0)
        {
            int i = random.nextInt(fortune + 2) - 1;

            if (i < 0)
            {
                i = 0;
            }

            return this.quantityDropped(random) * (i + 1);
        }
        else
        {
            return this.quantityDropped(random);
        }
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return ItemRegistry.CRYSTAL_FRAGMENT;
    }

    @Override
    public int damageDropped(IBlockState state)
    {
        return 0;
    }

    @Override
    public int getExpDrop(IBlockState state, IBlockAccess world, BlockPos pos, int fortune)
    {
        Random rand = new Random();
        int i = MathHelper.getInt(rand, 3, 7);

        if (fortune > 0)
        {
            i += MathHelper.getInt(rand, fortune, 5);
        }

        return i;
    }
}
