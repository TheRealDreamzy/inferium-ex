package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.BlockSlab;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.Random;

public abstract class BlockMetalSlab extends BlockSlab
{
    public static final PropertyEnum<BlockMetalSlab.EnumType> VARIANT = PropertyEnum.<BlockMetalSlab.EnumType>create("variant", BlockMetalSlab.EnumType.class);

    public BlockMetalSlab()
    {
        super(Material.IRON, MapColor.IRON);
        IBlockState iblockstate = this.blockState.getBaseState();
        if (!this.isDouble())
        {
            iblockstate = iblockstate.withProperty(HALF, BlockSlab.EnumBlockHalf.BOTTOM);
        }
        setDefaultState(iblockstate.withProperty(VARIANT, BlockMetalSlab.EnumType.IRON));
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
        setSoundType(SoundType.METAL);
        setUnlocalizedName("inferiumex.metal_slab");
    }

    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Item.getItemFromBlock(BlockRegistry.METAL_SLAB);
    }

    public ItemStack getItem(World worldIn, BlockPos pos, IBlockState state)
    {
        return new ItemStack(BlockRegistry.METAL_SLAB, 1, state.getValue(VARIANT).getMetadata());
    }

    public String getUnlocalizedName(int meta)
    {
        return super.getUnlocalizedName() + "." + BlockMetalSlab.EnumType.byMetadata(meta).getUnlocalizedName();
    }

    public IProperty<?> getVariantProperty()
    {
        return VARIANT;
    }

    public Comparable<?> getTypeForItem(ItemStack stack)
    {
        return BlockMetalSlab.EnumType.byMetadata(stack.getMetadata() & 7);
    }

    public IBlockState getStateFromMeta(int meta)
    {
        IBlockState iblockstate = this.getDefaultState().withProperty(VARIANT, BlockMetalSlab.EnumType.byMetadata(meta & 7));

        if (!this.isDouble())
        {
            iblockstate = iblockstate.withProperty(HALF, (meta & 8) == 0 ? BlockSlab.EnumBlockHalf.BOTTOM : BlockSlab.EnumBlockHalf.TOP);
        }

        return iblockstate;
    }

    public int getMetaFromState(IBlockState state)
    {
        int i = 0;
        i = i | state.getValue(VARIANT).getMetadata();

        if (!this.isDouble() && state.getValue(HALF) == BlockSlab.EnumBlockHalf.TOP)
        {
            i |= 8;
        }

        return i;
    }

    protected BlockStateContainer createBlockState()
    {
        return this.isDouble() ? new BlockStateContainer.Builder(this).add(VARIANT).build() : new BlockStateContainer.Builder(this).add(HALF, VARIANT).build();
    }

    public int damageDropped(IBlockState state)
    {
        return state.getValue(VARIANT).getMetadata();
    }

    public MapColor getMapColor(IBlockState state, IBlockAccess worldIn, BlockPos pos)
    {
        return state.getValue(VARIANT).getMapColor();
    }

    @Override
    public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
    {
        for (BlockMetalSlab.EnumType blockmetalslab$enumtype : BlockMetalSlab.EnumType.values())
        {
            items.add(new ItemStack(this, 1, blockmetalslab$enumtype.getMetadata()));
        }
    }

    public static enum EnumType implements IStringSerializable
    {
        IRON(0, MapColor.IRON, "iron"),
        GOLD(1, MapColor.GOLD, "gold"),
        DIAMOND(2, MapColor.DIAMOND, "diamond");

        private static final BlockMetalSlab.EnumType[] META_LOOKUP = new BlockMetalSlab.EnumType[values().length];
        private final int meta;
        private final MapColor mapColor;
        private final String name;
        private final String unlocalizedName;

        private EnumType(int meta, MapColor mapColor, String name)
        {
            this(meta, mapColor, name, name);
        }

        private EnumType(int meta, MapColor mapColor, String name, String unlocalizedName)
        {
            this.meta = meta;
            this.mapColor = mapColor;
            this.name = name;
            this.unlocalizedName = unlocalizedName;
        }

        public int getMetadata()
        {
            return this.meta;
        }

        public MapColor getMapColor()
        {
            return this.mapColor;
        }

        public String toString()
        {
            return this.name;
        }

        public static BlockMetalSlab.EnumType byMetadata(int meta)
        {
            if (meta < 0 || meta >= META_LOOKUP.length)
            {
                meta = 0;
            }

            return META_LOOKUP[meta];
        }

        public String getName()
        {
            return this.name;
        }

        public String getUnlocalizedName()
        {
            return this.unlocalizedName;
        }

        static
        {
            for (BlockMetalSlab.EnumType blockmetalslab$enumtype : values())
            {
                META_LOOKUP[blockmetalslab$enumtype.getMetadata()] = blockmetalslab$enumtype;
            }
        }
    }

    public static class Double extends BlockMetalSlab
    {
        public boolean isDouble()
        {
            return true;
        }
    }

    public static class Half extends BlockMetalSlab
    {
        public boolean isDouble()
        {
            return false;
        }
    }

    public static enum Variant implements IStringSerializable
    {
        IRON;

        public String getName()
        {
            return "iron";
        }
    }
}
