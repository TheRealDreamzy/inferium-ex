package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class HearaniumBlock extends Block
{
    public HearaniumBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(11.5F);
        setResistance(40.0F);
        setHarvestLevel("pickaxe", 4);
        setRegistryName("hearanium_block");
        setUnlocalizedName("inferiumex.hearanium_block");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
