package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class SapphiriteOre extends Block
{
    public SapphiriteOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(6.0F);
        setResistance(9.5F);
        setHarvestLevel("pickaxe", 3);
        setRegistryName("sapphirite_ore");
        setUnlocalizedName("inferiumex.sapphirite_ore");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
