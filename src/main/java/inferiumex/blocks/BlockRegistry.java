package inferiumex.blocks;

import inferiumex.blocks.barrel.*;
import inferiumex.tileentity.CrystalBlockTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSlab;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemSlab;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameRegistry.ObjectHolder("inferiumex")
@Mod.EventBusSubscriber(modid = "inferiumex")
public class BlockRegistry
{
    public static final Block SAPPHIRITE_ORE = Blocks.AIR;
    public static final Block HEARANIUM_ORE = Blocks.AIR;
    public static final Block TOPARITE_ORE = Blocks.AIR;
    public static final Block AMETHYTE_ORE = Blocks.AIR;
    public static final Block LARANIUM_ORE = Blocks.AIR;
    public static final Block CRYSTAL_ORE = Blocks.AIR;

    // Blocks out of ingots
    public static final Block SAPPHIRITE_BLOCK = Blocks.AIR;
    public static final Block HEARANIUM_BLOCK = Blocks.AIR;
    public static final Block TOPARITE_BLOCK = Blocks.AIR;
    public static final Block AMETHYTE_BLOCK = Blocks.AIR;
    public static final Block LARANIUM_BLOCK = Blocks.AIR;
    public static final Block CRYSTAL_BLOCK = Blocks.AIR;

    // Slabs
    public static final Block METAL_SLAB = Blocks.AIR;
    public static final Block DOUBLE_METAL_SLAB = Blocks.AIR;

    // Barrels
    public static final Block WOODEN_BARREL = Blocks.AIR;
    public static final Block STONE_BARREL = Blocks.AIR;
    public static final Block IRON_BARREL = Blocks.AIR;
    public static final Block GOLD_BARREL = Blocks.AIR;
    public static final Block DIAMOND_BARREL = Blocks.AIR;

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
    {
        event.getRegistry().register(new LaraniumOre());
        event.getRegistry().register(new AmethyteOre());
        event.getRegistry().register(new TopariteOre());
        event.getRegistry().register(new HearaniumOre());
        event.getRegistry().register(new SapphiriteOre());
        event.getRegistry().register(new CrystalOre());

        event.getRegistry().register(new LaraniumBlock());
        event.getRegistry().register(new AmethyteBlock());
        event.getRegistry().register(new TopariteBlock());
        event.getRegistry().register(new HearaniumBlock());
        event.getRegistry().register(new SapphiriteBlock());
        event.getRegistry().register(new CrystalBlock());

        event.getRegistry().register(new BlockMetalSlab.Half().setRegistryName("metal_slab"));
        event.getRegistry().register(new BlockMetalSlab.Double().setRegistryName("double_metal_slab"));

        event.getRegistry().register(new WoodenBarrel());
        event.getRegistry().register(new StoneBarrel());
        event.getRegistry().register(new IronBarrel());
        event.getRegistry().register(new GoldBarrel());
        event.getRegistry().register(new DiamondBarrel());
    }

    @SubscribeEvent
    public static void registerItemBlocks(RegistryEvent.Register<Item> event)
    {
        event.getRegistry().register(new ItemBlock(LARANIUM_ORE).setRegistryName(LARANIUM_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(AMETHYTE_ORE).setRegistryName(AMETHYTE_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(TOPARITE_ORE).setRegistryName(TOPARITE_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(HEARANIUM_ORE).setRegistryName(HEARANIUM_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(SAPPHIRITE_ORE).setRegistryName(SAPPHIRITE_ORE.getRegistryName()));
        event.getRegistry().register(new ItemBlock(CRYSTAL_ORE).setRegistryName(CRYSTAL_ORE.getRegistryName()));

        event.getRegistry().register(new ItemBlock(LARANIUM_BLOCK).setRegistryName(LARANIUM_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(AMETHYTE_BLOCK).setRegistryName(AMETHYTE_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(TOPARITE_BLOCK).setRegistryName(TOPARITE_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(HEARANIUM_BLOCK).setRegistryName(HEARANIUM_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(SAPPHIRITE_BLOCK).setRegistryName(SAPPHIRITE_BLOCK.getRegistryName()));
        event.getRegistry().register(new ItemBlock(CRYSTAL_BLOCK).setRegistryName(CRYSTAL_BLOCK.getRegistryName()));

        if (METAL_SLAB instanceof BlockSlab && DOUBLE_METAL_SLAB instanceof BlockSlab)
        {
            event.getRegistry().register(new ItemSlab(METAL_SLAB, (BlockSlab) METAL_SLAB, (BlockSlab) DOUBLE_METAL_SLAB).setRegistryName(METAL_SLAB.getRegistryName()));
        }

        event.getRegistry().register(new ItemBlock(WOODEN_BARREL).setRegistryName(WOODEN_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(STONE_BARREL).setRegistryName(STONE_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(IRON_BARREL).setRegistryName(IRON_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(GOLD_BARREL).setRegistryName(GOLD_BARREL.getRegistryName()));
        event.getRegistry().register(new ItemBlock(DIAMOND_BARREL).setRegistryName(DIAMOND_BARREL.getRegistryName()));

        // Tile Entities
        GameRegistry.registerTileEntity(CrystalBlockTileEntity.class, "inferiumex:crystal_block_tile_entity");
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void registerItemBlockModels(ModelRegistryEvent event)
    {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(LARANIUM_ORE), 0, new ModelResourceLocation(LARANIUM_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(AMETHYTE_ORE), 0, new ModelResourceLocation(AMETHYTE_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(TOPARITE_ORE), 0, new ModelResourceLocation(TOPARITE_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(HEARANIUM_ORE), 0, new ModelResourceLocation(HEARANIUM_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(SAPPHIRITE_ORE), 0, new ModelResourceLocation(SAPPHIRITE_ORE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(CRYSTAL_ORE), 0, new ModelResourceLocation(CRYSTAL_ORE.getRegistryName(), "inventory"));

        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(LARANIUM_BLOCK), 0, new ModelResourceLocation(LARANIUM_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(AMETHYTE_BLOCK), 0, new ModelResourceLocation(AMETHYTE_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(TOPARITE_BLOCK), 0, new ModelResourceLocation(TOPARITE_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(HEARANIUM_BLOCK), 0, new ModelResourceLocation(HEARANIUM_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(SAPPHIRITE_BLOCK), 0, new ModelResourceLocation(SAPPHIRITE_BLOCK.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(CRYSTAL_BLOCK), 0, new ModelResourceLocation(CRYSTAL_BLOCK.getRegistryName(), "inventory"));

        Item metalSlab = Item.getItemFromBlock(METAL_SLAB);
        for (BlockMetalSlab.EnumType variant : BlockMetalSlab.EnumType.values())
        {
            ModelLoader.setCustomModelResourceLocation(metalSlab, variant.getMetadata(), new ModelResourceLocation(metalSlab.getRegistryName(), "half=bottom,variant=" + variant.getName()));
        }

        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(WOODEN_BARREL), 0, new ModelResourceLocation(WOODEN_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(STONE_BARREL), 0, new ModelResourceLocation(STONE_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(IRON_BARREL), 0, new ModelResourceLocation(IRON_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(GOLD_BARREL), 0, new ModelResourceLocation(GOLD_BARREL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(DIAMOND_BARREL), 0, new ModelResourceLocation(DIAMOND_BARREL.getRegistryName(), "inventory"));

        GameRegistry.registerTileEntity(CrystalBlockTileEntity.class, "inferiumex:crystal_block_tile_entity");
    }
}
