package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class LaraniumBlock extends Block
{
    public LaraniumBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(13.0F);
        setResistance(44.0F);
        setHarvestLevel("pickaxe", 7);
        setRegistryName("laranium_block");
        setUnlocalizedName("inferiumex.laranium_block");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
