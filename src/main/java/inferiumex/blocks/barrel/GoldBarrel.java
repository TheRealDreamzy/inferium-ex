package inferiumex.blocks.barrel;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;

public class GoldBarrel extends Block
{
    public GoldBarrel()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(1.5F);
        setResistance(5.0F);
        setHarvestLevel("pickaxe", 2);
        setRegistryName("gold_barrel");
        setUnlocalizedName("inferiumex.gold_barrel");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
}
