package inferiumex.blocks.barrel;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;

public class StoneBarrel extends Block
{
    public StoneBarrel()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(1.5F);
        setResistance(5.0F);
        setHarvestLevel("pickaxe", 1);
        setRegistryName("stone_barrel");
        setUnlocalizedName("inferiumex.stone_barrel");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
}
