package inferiumex.blocks.barrel;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockRenderLayer;

public class WoodenBarrel extends Block
{
    public WoodenBarrel()
    {
        super(Material.WOOD);
        setSoundType(SoundType.WOOD);
        setHardness(1.5F);
        setResistance(5.0F);
        setHarvestLevel("axe", 1);
        setRegistryName("wooden_barrel");
        setUnlocalizedName("inferiumex.wooden_barrel");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }
}
