package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class LaraniumOre extends Block
{
    public LaraniumOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(8.0F);
        setResistance(15.0F);
        setHarvestLevel("pickaxe", 7);
        setRegistryName("laranium_ore");
        setUnlocalizedName("inferiumex.laranium_ore");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
