package inferiumex.blocks;

import inferiumex.InferiumEx;
import inferiumex.InferiumExTab;
import inferiumex.proxy.GuiProxy;
import inferiumex.tileentity.CrystalBlockTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class CrystalBlock extends Block
{
    public CrystalBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.GLASS);
        setHardness(5.0F);
        setResistance(45.0F);
        setHarvestLevel("pickaxe", 3);
        setRegistryName("crystal_block");
        setUnlocalizedName("inferiumex.crystal_block");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);

    }

    @Override
    public TileEntity createTileEntity(World world, IBlockState state)
    {
        return new CrystalBlockTileEntity();
    }

    @Override
    public boolean hasTileEntity(IBlockState state)
    {
        return true;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state)
    {
        return false;
    }

    @Override
    public BlockRenderLayer getBlockLayer()
    {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
    {
        TileEntity tileEntity = worldIn.getTileEntity(pos);
        // Drop inventory
        if (tileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null))
        {
            IItemHandler inventory = tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null);
            // Get and then drop items from the inv slots
            for (int i = 0; i < inventory.getSlots(); ++i)
            {
                ItemStack itemStack = inventory.getStackInSlot(i);
                if (!itemStack.isEmpty())
                {
                    EntityItem entityitem = new EntityItem(worldIn, pos.getX(), pos.getY(), pos.getZ(), itemStack);
                    entityitem.setPickupDelay(0);
                    worldIn.spawnEntity(entityitem);
                }
            }
        }
        super.breakBlock(worldIn, pos, state);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if (!worldIn.isRemote)
        {
            if (worldIn.getTileEntity(pos) instanceof CrystalBlockTileEntity)
            {
                playerIn.openGui(InferiumEx.instance, GuiProxy.CRYSTAL, worldIn, pos.getX(), pos.getY(), pos.getZ());
            }
        }
        return true;
    }
}
