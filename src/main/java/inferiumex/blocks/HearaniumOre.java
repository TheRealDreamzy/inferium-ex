package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class HearaniumOre extends Block
{
    public HearaniumOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(6.5F);
        setResistance(10.0F);
        setHarvestLevel("pickaxe", 4);
        setRegistryName("hearanium_ore");
        setUnlocalizedName("inferiumex.hearanium_ore");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
