package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class TopariteBlock extends Block
{
    public TopariteBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(11.75F);
        setResistance(41.5F);
        setHarvestLevel("pickaxe", 5);
        setRegistryName("toparite_block");
        setUnlocalizedName("inferiumex.toparite_block");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
