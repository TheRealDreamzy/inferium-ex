package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class SapphiriteBlock extends Block
{
    public SapphiriteBlock()
    {
        super(Material.IRON);
        setSoundType(SoundType.METAL);
        setHardness(11.0F);
        setResistance(39.0F);
        setHarvestLevel("pickaxe", 3);
        setRegistryName("sapphirite_block");
        setUnlocalizedName("inferiumex.sapphirite_block");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
