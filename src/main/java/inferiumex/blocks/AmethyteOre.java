package inferiumex.blocks;

import inferiumex.InferiumExTab;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class AmethyteOre extends Block
{
    public AmethyteOre()
    {
        super(Material.ROCK);
        setSoundType(SoundType.STONE);
        setHardness(7.5F);
        setResistance(14.0F);
        setHarvestLevel("pickaxe", 6);
        setRegistryName("amethyte_ore");
        setUnlocalizedName("inferiumex.amethyte_ore");
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
