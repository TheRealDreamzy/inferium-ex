package inferiumex.items;

import inferiumex.InferiumExTab;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;

public class PickaxeBase extends ItemPickaxe
{
    public PickaxeBase(String name, Item.ToolMaterial material)
    {
        super(material);
        setRegistryName(name);
        setUnlocalizedName("inferiumex." + name);
        setHarvestLevel("pickaxe", material.getHarvestLevel());
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
