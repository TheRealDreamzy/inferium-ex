package inferiumex.items;

import inferiumex.InferiumEx;
import inferiumex.InferiumExTab;
import inferiumex.util.MatRefs;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


@GameRegistry.ObjectHolder("inferiumex")
@Mod.EventBusSubscriber(modid = "inferiumex")
public class ItemRegistry
{
    // Laranium
    public static final Item LARANIUM_INGOT = Items.AIR;
    public static final Item LARANIUM_NUGGET = Items.AIR;
    public static final Item LARANIUM_AXE = Items.AIR;
    public static final Item LARANIUM_PICKAXE = Items.AIR;
    public static final Item LARANIUM_SHOVEL = Items.AIR;
    public static final Item LARANIUM_SWORD = Items.AIR;

    // Amethyte
    public static final Item AMETHYTE_INGOT = Items.AIR;
    public static final Item AMETHYTE_NUGGET = Items.AIR;
    public static final Item AMETHYTE_AXE = Items.AIR;
    public static final Item AMETHYTE_PICKAXE = Items.AIR;
    public static final Item AMETHYTE_SHOVEL = Items.AIR;
    public static final Item AMETHYTE_SWORD = Items.AIR;

    // Toparite
    public static final Item TOPARITE_INGOT = Items.AIR;
    public static final Item TOPARITE_NUGGET = Items.AIR;
    public static final Item TOPARITE_AXE = Items.AIR;
    public static final Item TOPARITE_PICKAXE = Items.AIR;
    public static final Item TOPARITE_SHOVEL = Items.AIR;
    public static final Item TOPARITE_SWORD = Items.AIR;

    // Hearanium
    public static final Item HEARANIUM_INGOT = Items.AIR;
    public static final Item HEARANIUM_NUGGET = Items.AIR;
    public static final Item HEARANIUM_AXE = Items.AIR;
    public static final Item HEARANIUM_PICKAXE = Items.AIR;
    public static final Item HEARANIUM_SHOVEL = Items.AIR;
    public static final Item HEARANIUM_SWORD = Items.AIR;

    // Sapphirite
    public static final Item SAPPHIRITE_INGOT = Items.AIR;
    public static final Item SAPPHIRITE_NUGGET = Items.AIR;
    public static final Item SAPPHIRITE_AXE = Items.AIR;
    public static final Item SAPPHIRITE_PICKAXE = Items.AIR;
    public static final Item SAPPHIRITE_SHOVEL = Items.AIR;
    public static final Item SAPPHIRITE_SWORD = Items.AIR;

    // Crystal
    public static final Item CRYSTAL_FRAGMENT = Items.AIR;
    public static final Item CRYSTAL_FRAME_FRAGMENT = Items.AIR;

    public static final Item OBSIDIAN_STICK = Items.AIR;

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
    {
        // Laranium Items
        Item laranium_ingot = new Item().setRegistryName("laranium_ingot").setUnlocalizedName("inferiumex.laranium_ingot").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
        event.getRegistry().register(laranium_ingot);
        MatRefs.LARANIUM.setRepairItem(new ItemStack(laranium_ingot));
        event.getRegistry().register(new Item().setRegistryName("laranium_nugget").setUnlocalizedName("inferiumex.laranium_nugget").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new AxeBase("laranium_axe", MatRefs.LARANIUM, 11.0F, -3.1F).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new PickaxeBase("laranium_pickaxe", MatRefs.LARANIUM).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.LARANIUM).setRegistryName("laranium_shovel").setUnlocalizedName("inferiumex.laranium_shovel").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.LARANIUM).setRegistryName("laranium_sword").setUnlocalizedName("inferiumex.laranium_sword").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));

        // Amethyte Items
        Item amethyte_ingot = new Item().setRegistryName("amethyte_ingot").setUnlocalizedName("inferiumex.amethyte_ingot").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
        event.getRegistry().register(amethyte_ingot);
        MatRefs.AMETHYTE.setRepairItem(new ItemStack(amethyte_ingot));
        event.getRegistry().register(new Item().setRegistryName("amethyte_nugget").setUnlocalizedName("inferiumex.amethyte_nugget").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new AxeBase("amethyte_axe", MatRefs.AMETHYTE, 6.5F, -3.1F).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new PickaxeBase("amethyte_pickaxe", MatRefs.AMETHYTE).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.AMETHYTE).setRegistryName("amethyte_shovel").setUnlocalizedName("inferiumex.amethyte_shovel").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.AMETHYTE).setRegistryName("amethyte_sword").setUnlocalizedName("inferiumex.amethyte_sword").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));

        // Toparite
        Item toparite_ingot = new Item().setRegistryName("toparite_ingot").setUnlocalizedName("inferiumex.toparite_ingot").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
        event.getRegistry().register(toparite_ingot);
        MatRefs.TOPARITE.setRepairItem(new ItemStack(toparite_ingot));
        event.getRegistry().register(new Item().setRegistryName("toparite_nugget").setUnlocalizedName("inferiumex.toparite_nugget").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new AxeBase("toparite_axe", MatRefs.TOPARITE, 6.0F, -3.1F).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new PickaxeBase("toparite_pickaxe", MatRefs.TOPARITE).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.TOPARITE).setRegistryName("toparite_shovel").setUnlocalizedName("inferiumex.toparite_shovel").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.TOPARITE).setRegistryName("toparite_sword").setUnlocalizedName("inferiumex.toparite_sword").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));

        // Hearanium Items
        Item hearanium_ingot = new Item().setRegistryName("hearanium_ingot").setUnlocalizedName("inferiumex.hearanium_ingot").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
        event.getRegistry().register(hearanium_ingot);
        MatRefs.HEARANIUM.setRepairItem(new ItemStack(hearanium_ingot));
        event.getRegistry().register(new Item().setRegistryName("hearanium_nugget").setUnlocalizedName("inferiumex.hearanium_nugget").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new AxeBase("hearanium_axe", MatRefs.HEARANIUM, 5.7F, -3.1F).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new PickaxeBase("hearanium_pickaxe", MatRefs.HEARANIUM).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.HEARANIUM).setRegistryName("hearanium_shovel").setUnlocalizedName("inferiumex.hearanium_shovel").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.HEARANIUM).setRegistryName("hearanium_sword").setUnlocalizedName("inferiumex.hearanium_sword").setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));

        // Sapphirite Items
        Item sapphirite_ingot = new Item().setRegistryName("sapphirite_ingot").setUnlocalizedName("inferiumex.sapphirite_ingot").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
        event.getRegistry().register(sapphirite_ingot);
        MatRefs.SAPPHIRITE.setRepairItem(new ItemStack(sapphirite_ingot));
        event.getRegistry().register(new Item().setRegistryName("sapphirite_nugget").setUnlocalizedName("inferiumex.sapphirite_nugget").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new AxeBase("sapphirite_axe", MatRefs.SAPPHIRITE, 5.0F, -3.1F).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new PickaxeBase("sapphirite_pickaxe", MatRefs.SAPPHIRITE).setMaxStackSize(1).setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSpade(MatRefs.SAPPHIRITE).setRegistryName("sapphirite_shovel").setUnlocalizedName("inferiumex.sapphirite_shovel").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new ItemSword(MatRefs.SAPPHIRITE).setRegistryName("sapphirite_sword").setUnlocalizedName("inferiumex.sapphirite_sword").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));

        // Crystal Items
        event.getRegistry().register(new Item().setRegistryName("crystal_fragment").setUnlocalizedName("inferiumex.crystal_fragment").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
        event.getRegistry().register(new Item().setRegistryName("crystal_frame_fragment").setUnlocalizedName("inferiumex.crystal_frame_fragment").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));

        // Items
        event.getRegistry().register(new Item().setRegistryName("obsidian_stick").setUnlocalizedName("inferiumex.obsidian_stick").setCreativeTab(InferiumExTab.INFERIUM_EX_TAB));
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public static void registerItemModels(ModelRegistryEvent event)
    {
        // Laranium
        ModelLoader.setCustomModelResourceLocation(LARANIUM_INGOT, 0, new ModelResourceLocation(LARANIUM_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_NUGGET, 0, new ModelResourceLocation(LARANIUM_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_AXE, 0, new ModelResourceLocation(LARANIUM_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_PICKAXE, 0, new ModelResourceLocation(LARANIUM_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_SHOVEL, 0, new ModelResourceLocation(LARANIUM_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(LARANIUM_SWORD, 0, new ModelResourceLocation(LARANIUM_SWORD.getRegistryName(), "inventory"));

        // Amethyte
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_INGOT, 0, new ModelResourceLocation(AMETHYTE_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_NUGGET, 0, new ModelResourceLocation(AMETHYTE_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_AXE, 0, new ModelResourceLocation(AMETHYTE_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_PICKAXE, 0, new ModelResourceLocation(AMETHYTE_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_SHOVEL, 0, new ModelResourceLocation(AMETHYTE_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(AMETHYTE_SWORD, 0, new ModelResourceLocation(AMETHYTE_SWORD.getRegistryName(), "inventory"));

        // Toparite
        ModelLoader.setCustomModelResourceLocation(TOPARITE_INGOT, 0, new ModelResourceLocation(TOPARITE_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_NUGGET, 0, new ModelResourceLocation(TOPARITE_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_AXE, 0, new ModelResourceLocation(TOPARITE_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_PICKAXE, 0, new ModelResourceLocation(TOPARITE_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_SHOVEL, 0, new ModelResourceLocation(TOPARITE_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(TOPARITE_SWORD, 0, new ModelResourceLocation(TOPARITE_SWORD.getRegistryName(), "inventory"));

        // Hearanium
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_INGOT, 0, new ModelResourceLocation(HEARANIUM_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_NUGGET, 0, new ModelResourceLocation(HEARANIUM_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_AXE, 0, new ModelResourceLocation(HEARANIUM_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_PICKAXE, 0, new ModelResourceLocation(HEARANIUM_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_SHOVEL, 0, new ModelResourceLocation(HEARANIUM_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(HEARANIUM_SWORD, 0, new ModelResourceLocation(HEARANIUM_SWORD.getRegistryName(), "inventory"));

        // Sapphirite
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_INGOT, 0, new ModelResourceLocation(SAPPHIRITE_INGOT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_NUGGET, 0, new ModelResourceLocation(SAPPHIRITE_NUGGET.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_AXE, 0, new ModelResourceLocation(SAPPHIRITE_AXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_PICKAXE, 0, new ModelResourceLocation(SAPPHIRITE_PICKAXE.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_SHOVEL, 0, new ModelResourceLocation(SAPPHIRITE_SHOVEL.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(SAPPHIRITE_SWORD, 0, new ModelResourceLocation(SAPPHIRITE_SWORD.getRegistryName(), "inventory"));

        // Crystal
        ModelLoader.setCustomModelResourceLocation(CRYSTAL_FRAGMENT, 0, new ModelResourceLocation(CRYSTAL_FRAGMENT.getRegistryName(), "inventory"));
        ModelLoader.setCustomModelResourceLocation(CRYSTAL_FRAME_FRAGMENT, 0, new ModelResourceLocation(CRYSTAL_FRAME_FRAGMENT.getRegistryName(), "inventory"));

        // Items
        ModelLoader.setCustomModelResourceLocation(OBSIDIAN_STICK, 0, new ModelResourceLocation(OBSIDIAN_STICK.getRegistryName(), "inventory"));
    }

}
