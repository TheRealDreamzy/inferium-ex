package inferiumex.items;

import inferiumex.InferiumExTab;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;

public class AxeBase extends ItemAxe
{
    public AxeBase(String name, Item.ToolMaterial material, float damage, float speed)
    {
        super(material, damage, speed);
        setRegistryName(name);
        setUnlocalizedName("inferiumex." + name);
        setHarvestLevel("axe", material.getHarvestLevel());
        setCreativeTab(InferiumExTab.INFERIUM_EX_TAB);
    }
}
